# Machine Learning: Backprop/ Computational Evolution algorithms
A repository for Ross Wendt's genetic algorithms project for Dr. Sheppard's Machine Learning: Soft Computing course at MSU, Bozeman. Started as a Fall 2015 class project for Dr. Sheppard's Machine Learning: Soft computing class at MSU Bozeman, MT. Credit to Angus T. and Lukas K. for the work we did during that time. 

# Neural Nets with Matrices
Functionality for the ANN/ backprop/ feed-forward with matrix math implemented thanks to [Brian Dolhansky's Artificial Neural Networks: Matrix Form (Part 5)](http://briandolhansky.com/blog/2014/10/30/artificial-neural-networks-matrix-form-part-5).

TODO: CUDA/Open-CL -ize the matrix code so this method is worth it. :)



# Dataset Notes

Example data taken from UCI ML repo. Some sets are missing (from my bitbucket repo), but info.txt should guide the way.

# Example Output


```
#!java
EPOCH 36:
k = 0	1.36(s) 	Training Error:1.0240188879e+02		Testing Error:	1.1487077441e+02
k = 1	1.36(s) 	Training Error:1.0207140022e+02		Testing Error:	1.0213718559e+02
k = 2	1.36(s) 	Training Error:1.0170026609e+02		Testing Error:	1.1009350695e+02
k = 3	1.37(s) 	Training Error:1.0136874708e+02		Testing Error:	9.8950687041e+01
k = 4	1.37(s) 	Training Error:1.0089906817e+02		Testing Error:	1.1189439063e+02
k = 5	1.37(s) 	Training Error:1.0057029262e+02		Testing Error:	9.9784589626e+01
k = 6	1.38(s) 	Training Error:1.0020105331e+02		Testing Error:	1.0176176485e+02
k = 7	1.38(s) 	Training Error:9.9735533021e+01		Testing Error:	1.0372129098e+02
k = 8	1.39(s) 	Training Error:9.9400472155e+01		Testing Error:	9.8654815576e+01
k = 9	1.39(s) 	Training Error:9.9012774909e+01		Testing Error:	1.1594576141e+02
EPOCH 37:
k = 0	1.39(s) 	Training Error:9.8697813245e+01		Testing Error:	1.1118241255e+02
k = 1	1.39(s) 	Training Error:9.8380584220e+01		Testing Error:	9.8431094528e+01
k = 2	1.40(s) 	Training Error:9.8022692119e+01		Testing Error:	1.0643432902e+02
k = 3	1.40(s) 	Training Error:9.7704343112e+01		Testing Error:	9.5293476737e+01
k = 4	1.40(s) 	Training Error:9.7247885153e+01		Testing Error:	1.0825696637e+02
k = 5	1.41(s) 	Training Error:9.6932188599e+01		Testing Error:	9.6138187618e+01
k = 6	1.41(s) 	Training Error:9.6576012203e+01		Testing Error:	9.8133259254e+01
k = 7	1.41(s) 	Training Error:9.6123579308e+01		Testing Error:	1.0011512558e+02
k = 8	1.42(s) 	Training Error:9.5801469457e+01		Testing Error:	9.5064322360e+01
k = 9	1.42(s) 	Training Error:9.5426717258e+01		Testing Error:	1.1240257219e+02
EPOCH 38:
k = 0	1.42(s) 	Training Error:9.5124604369e+01		Testing Error:	1.0762510568e+02
k = 1	1.43(s) 	Training Error:9.4820181925e+01		Testing Error:	9.4854815599e+01
k = 2	1.43(s) 	Training Error:9.4475082725e+01		Testing Error:	1.0290551578e+02
k = 3	1.43(s) 	Training Error:9.4169454158e+01		Testing Error:	9.1766142865e+01
k = 4	1.44(s) 	Training Error:9.3725776750e+01		Testing Error:	1.0474878013e+02
k = 5	1.44(s) 	Training Error:9.3422712837e+01		Testing Error:	9.2620104437e+01
k = 6	1.44(s) 	Training Error:9.3079156875e+01		Testing Error:	9.4632765499e+01
k = 7	1.45(s) 	Training Error:9.2639375945e+01		Testing Error:	9.6636813294e+01
k = 8	1.45(s) 	Training Error:9.2329776103e+01		Testing Error:	9.1601497774e+01
k = 9	1.45(s) 	Training Error:9.1967532565e+01		Testing Error:	1.0898702426e+02
```