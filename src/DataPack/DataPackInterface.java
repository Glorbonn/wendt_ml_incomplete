package DataPack;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ross Wendt
 */
public abstract class DataPackInterface {

    public double[][] initializeYDataSet(double[][] xDataSet, int colNumber) {
        double[][] yDataSet = new double[xDataSet.length][1];
        for (int i = 0; i < yDataSet.length; i++) {
            yDataSet[i] = computeFunction(xDataSet[i]);
        }
        return yDataSet;
    }
    
    abstract double[] computeFunction(double[] x);

    public double[][] initializeXDataSet(int samples, int n, double lowerBound, double upperBound) {
        Random rdm = new Random();
        double[][] xDataSet = new double[samples][n];
        for (int i = 0; i < samples; i++) {
            for (int j = 0; j < n; j++) {
                xDataSet[i][j] = rdm.nextDouble() * (upperBound - lowerBound) + lowerBound;
            }
        }
        return xDataSet;
    }    
}
