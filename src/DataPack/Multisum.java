package DataPack;

/**
 *
 * @author Ross Wendt
 */
public class Multisum extends DataPackInterface {
    
    @Override
    public double[] computeFunction(double[] x) {
    double[] outVal = new double[DriverPack.Driver.outputSize];
        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < outVal.length; j++ ) {
                //System.out.println(x[i]);
                outVal[j] += x[i];
                //System.out.println(outVal[j]);
        }

        }
        return outVal;
    }
}
