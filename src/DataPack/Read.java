package DataPack;
import DataPack.DataPackInterface;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Ross Wendt
 */
public class Read extends DataPackInterface {
    File in;
    List<Integer> cols;
    ArrayList<ArrayList<Double>> readToArray = new ArrayList<>();
    public Read(String path, List<Integer> skipCol) {
        in = new File(path);
        cols = skipCol;
    }
    
    public void readToInput() throws FileNotFoundException, IOException {
        readToArray = new ArrayList<>();
        ArrayList<Double> A = new ArrayList<>();
        final String delim = ",";
        int counter = 0;
        BufferedReader reader = new BufferedReader(new FileReader(in));
        //reader.readLine(); // skip first line
        String line = reader.readLine();
        while (line != null) {
            counter++;
            //if (counter % 1000 == 0)
                //System.out.println(counter);
            String[] read = line.split(delim);
            A = new ArrayList<>();
            for (String s : read ) {
                if (!s.contains("-") && !s.equals("")) {
                    A.add(Double.parseDouble(s));
                }
            }
                
            line = reader.readLine();
            //line = reader.readLine();
            if(readToArray.contains(A)) {
                System.out.println("DUPLICATE ERROR");
            }
            readToArray.add(A);
        }    
    }
    
    @Override
    public double[][] initializeXDataSet(int colExclude, int notUsed1, double notUsed2, double notUsed3) {
            boolean case1 = false;
            int check2 = 0;
        try {
            //ArrayList<ArrayList<Double>> read = new ArrayList<>();
            /*(try {
            read = readToInput();
            } catch (IOException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            readToInput();
        } catch (IOException ex) {
            Logger.getLogger(Read.class.getName()).log(Level.SEVERE, null, ex);
        }

        //ArrayList<ArrayList<Double>> copy = new ArrayList<>(readToArray);
        System.out.println(readToArray.size() + " " + readToArray.get(0).size());
        double[][] xDataSet = new double[readToArray.size()][readToArray.get(0).size()]; 
        System.out.println(xDataSet.length + " " + xDataSet[0].length);
        //double[][] xDataSet = new double[read.get(0).size()-1][read.size()];
        //int check = copy.size();
        for (int i = 0; i < readToArray.size(); i++) {
            for (int j = 0; j < readToArray.get(0).size(); j++) {
                if (!cols.contains(j) && readToArray.get(i).size() == readToArray.get(0).size()) {
                    //System.out.println(readToArray.get(i).size());
                    xDataSet[i][j] = readToArray.get(i).get(j);
                }

            }
        }

        return xDataSet;
    }
    
    @Override
    public double[][] initializeYDataSet(double[][] read, int colNumber) {     
        
        double[][] yDataSet = new double[readToArray.size()][cols.size()];
        for (int i = 0; i < readToArray.size(); i++) {
            for (int j = 0; j < readToArray.get(0).size(); j++) { 
                for (int k = 0; k < cols.size(); k++) {
                    if (cols.contains(j) && readToArray.get(i).size() == readToArray.get(0).size()) {
                        yDataSet[i][k] = readToArray.get(i).get(j);
                    }
                }
            }
        }
        return yDataSet;
    }
    
    @Override 
    double[] computeFunction(double[] x) {
        System.out.println("ERROR: THIS SHOULD NOT APPEAR!"); // :)
        double[] d = new double[1];
        return d;
    };
}
