package DataPack;

/**
 *
 * @author Ross Wendt
 */
public class Rosenbrock extends DataPackInterface {
    
    /*****CONTAINS ROSENBROCK STUF
     * @param x
     * @param xF*    
     * @return ****/    
    
 
    
    @Override
    public double[] computeFunction(double[] x) {
    double[] y = new double[1];
    y[0] = 0;
        for (int i = 0; i < x.length - 1; i++) {
            y[0] += 100 * Math.pow(x[i+1] - Math.pow(x[i],2),2) + Math.pow(1 - x[i], 2);
        }
        return y;
    }
    


}
