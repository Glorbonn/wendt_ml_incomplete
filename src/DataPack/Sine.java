package DataPack;

/**
 *
 * @author Ross Wendt
 */
public class Sine extends DataPackInterface{

    @Override
    double[] computeFunction(double[] x) {
        double[] d = new double[1];
        d[0] = Math.sin(x[0]);
        return d;
    }
    
}
