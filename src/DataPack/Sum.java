package DataPack;
/**
 *
 * @author Ross Wendt
 */
public class Sum extends DataPackInterface {
    
    /***** CONTAINS SIMPLE SUM FUNCTION TO TEST NEURAL NET*****/


    
    @Override
    public double[] computeFunction(double[] x) {
    double outVal[] = new double[1];
        for (int i = 0; i < x.length; i++) {
            outVal[0] += x[i];
        }
        return outVal;
    }

}
