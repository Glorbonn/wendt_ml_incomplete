package DriverPack;

import java.io.IOException;

/**
 *
 * @author Ross Wendt
 */
public class AutoDriver {

    public static void main(String[] args) throws IOException {
        //Read R = new Read("F:\\Google Drive\\github stuff\\MSUMLSheppard2015\\UCI_data\\airfoil\\use\\airfoil_self_noise.dat");
        //ArrayList<ArrayList<String>> airfoil = R.readToInput();
        for (int i =0; i < 1; i++) {
            //System.out.println("TRIAL " + i + ":");
            //System.out.println("RUNNING WITH ETA: " + Driver.eta);
            Driver D = new Driver();
            //D.trainingMethod = new Backpropagation();
            D.run();
            //System.out.println("END TRIAL" + i + ":");   
        }
    }
}
