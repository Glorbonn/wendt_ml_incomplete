package DriverPack;
import DataPack.DataPackInterface;
import DataPack.Rosenbrock;
import DataPack.Read;
import NeuralNet.NeuralNetDriver;
import Math.ActivationFunctions.*;
import NeuralNet.*;
import Math.Matrix;
import NeuralNet.TrainingMethod.*;
import NeuralNet.TrainingMethod.Genetic.DifferentialEvolution;
import NeuralNet.TrainingMethod.Genetic.EvolutionStrategy;
import NeuralNet.TrainingMethod.Genetic.GeneticAlgorithm;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author Ross Wendt/
 */
public class Driver {   

    //epochLimit+=1;
    /*
    DE TUNABLE PARAMS:
     */
    public static double beta = 0.05; //scaling "amplification" factor used in DE mutation
    public static int outputStep = 20;
    public static int epochLimit = 21;
    


    public static int populationSize = 20; //initial population size
    public static int populationMax = 100; //population maximum size
    public static double mutationRate = 0.2;
    public static double crossoverRate = 0.1;
    
    public static int mu = 4; //parent population size
    public static int lambda = 2; //number of children
    
        
    /*RBF TUNABLE PARAMS:
    
    ***ONLY USED FOR RBF*****
    Chose center and radius here
    */
    
    static double radius = 1.0;
    static double center = -9.0;
    
        
    /*
    BACKPROP TUNABLE PARAMS:
    Tunable parameters for the Neural Net with backprop are as follows:
     */
    public static double eta = 0.00001;
    public static double momentumParameter = .9; 

   
    /*
    REGRESSION PARAMETERS
    
    *****************************************************************
    ********THESE 4 PARAMETERS ARE ONLY USED FOR REGRESSION**********
    ***SKIP TO NEXT SECTION IF DATA FILES WILL BE USED FOR INPUT*****
    *** !!!!!!!!!!DATA INPUT NOT YET IMPLEMENTED!!!!!!!!!       *****
    *****************************************************************
    
    Changes these values to affect the regression parameters;
    */

    public static double xValLowerBound = -1.0;
    public static double xValUpperBound = 0.0;
    public static int dataSetSize = 1000; // make sure this number is divisible by k
    public static int dimensionality = 6; 
    
    
    public static double exponent = 1.00; //not used
    public static double upperBoundWeight = 1000.0; //sets the upperbound for randomly intializing weights
    public static double upperBoundBiasWeight = 100.0; //sets the upper bound for a random bias weight on all nodes
    public static int outputSize = 1;       // must change this in tandem with the test function: if test func outputs r^n, then this should be n
    public static int[] hiddenLayers = {20,outputSize}; 


    /*
    IN/OUT VALUE SELECTION
    
    Select whether to generate input values, or use a file for input.
    Also, select whether to use an data function for regression, or use a file
    to select data values (for datasets from repository). Choose training method
    here too.
    */
    
    static AbstractFunction activationFunction = new HyperbolicTangent();//new Gaussian(0.1,0.1);
    public static TrainingMethodInterface trainingMethod = new Backpropagation();
    //static GenerateInputValsInterface input = new Read("");
    //input.
    //static GenerateOutputValsInterface = input.getVals();
    
    /*
    File loading information
    */
    static String airfoil = "UCI_data\\airfoil\\use\\airfoil_self_noise.dat";
    static String concrete = "UCI_data\\concrete\\use\\Concrete_Data.csv";
    static String energy = "UCI_data\\energy\\use\\ENB2012_data.csv";
    static String gas = "UCI_data\\gas\\use\\000_Et_H_CO_n";
    static String redwine = "UCI_data\\wine\\use\\winequality-red.csv";
    static String slump = "UCI_data\\slump\\use\\slump_test.data";
    static String yacht = "UCI_data\\yacht\\use\\yacht_hydrodynamics.data";
    static String fires = "UCI_data\\fires\\use\\forestfires.csv";
    static String music = "UCI_data\\music\\use\\tracks.txt";
    static String cbm = "UCI_data\\cbm\\use\\cbm.txt";
    /*
    exclude certain elements from the input file to be used as target output.
    */
    
    static List<Integer> exclude_concrete = asList(5);
    static List<Integer> exclude_airfoil = asList(5);
    static List<Integer> exclude_energy = asList(5);
    static List<Integer> exclude_gas = asList(1);
    static List<Integer> exclude_redwine = asList(11);
    static List<Integer> exclude_slump = asList(9);
    static List<Integer> exclude_yacht = asList(6);
    static List<Integer> exclude_fires = asList(12);
    static List<Integer> exclude_music = asList(25);
    static List<Integer> exclude_cbm = asList(14);
    
    
    
    //static final DataPackInterface data = new Read(concrete, exclude_conc);
    //static final DataPackInterface data = new Read(airfoil, exclude_gas);
    static final DataPackInterface data = new Rosenbrock();//new Read(slump, exclude_slump);

    
    /*
    K FOLDS CROSS VALIDATION:
    
    k sets the number of folds for k-fold cross validation.
    */
    public static int repetitions = 2;
    public static int k = 10; // number of folds


    /*
    Program data parameters
    */

    public static boolean runWithOutput = true; //NOT YET IMPLEMENTED

    /*************************************************************************/
    /* Setup below for various vars that shouldn't need to be changed        */
    /*************************************************************************/
    
    public static double[][] xDataSet = data.initializeXDataSet(dataSetSize, dimensionality, xValLowerBound, xValUpperBound);
    public static double[][] yDataSet = data.initializeYDataSet(xDataSet, 0);
    
    //public static double[][] xDataSet = data.initializeXDataSet(dataSetSize, dimensionality, xValLowerBound, xValUpperBound);
    //public static double[][] yDataSet = data.initializeYDataSet(xDataSet, 0);
    
    //public static boolean isHiddenLayerZero = false;
    public static Matrix meanSquaredError;     
    public static int[][] subsets = KFolds.initializeSubsets(dataSetSize, k);
    public static int meansSquaredErrorDivisor = (k - 1) * (subsets[0].length);  
    public static double[][] inputLayer = xDataSet;
    public static double[][] outputLayer = yDataSet;
    public static double[][] targetOutput = yDataSet;
    public static Matrix meanSquaredErrorTraining;
    public static Matrix meanSquaredErrorTesting;            
    public static MatrixNeuralNet nNet = new MatrixNeuralNet(inputLayer, targetOutput, hiddenLayers, upperBoundWeight, upperBoundBiasWeight, eta, exponent, epochLimit, momentumParameter, epochLimit, activationFunction, trainingMethod);
    public static NeuralNetDriver nNetHelper = new NeuralNetDriver(nNet, outputStep);
    
    
    /*
    
    MatrixNeuralNet parameters order:

    double[] input, double[] targetOutput, int[] hiddenLayers, double upperBoundInitializationWeight, 
    double upperBoundInitializationBias, double eta, double momentumParameter, int inEpochLimit, 
    AbstractFunction inActivationFunctionInterface,
    TrainingMethodInterface inTrainingMethodInterface

    */


    
    public void run() {        
    if (yDataSet[0].length != outputSize ) {
        System.out.println("Target Output & Predicted Output size mismatch: dimensions on the Y Data Set do not match specified Neural Net Output Size");
        System.exit(1);
    }
            while (true) { //this lets us break if DataSet size is not divisible by K
                if (dataSetSize % k != 0 ) {
                    System.out.println("!!!!ERROR: Dataset size is NOT divisble by k!!!!");
                    break;
                }
                //if (dataSetSize % populationSize != 0 ) {
                //    System.out.println("ERROR POP SIZE AND DATASET SIZE INCOMPATIBLE");
                //    break;
                //}
                nNetHelper.runNeuralNet(); //initializes and runs NNET with x-validate
                break; //gets us out of the loop
        }

    }
        
    public static MatrixNeuralNet getNeuralNet() {
            return nNet;
        }

    public static int getDimension() {
        return dimensionality;
    }
    
    public static NeuralNetDriver getNetDriver() {
        return nNetHelper;
    }

    static void setOutputStep(int i) {
        outputStep = i;
    }
}