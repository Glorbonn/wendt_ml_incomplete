package Math.ActivationFunctions;
/**
 *
 * @author Ross Wendt/ Lukas Keller
 */
public class Gaussian extends AbstractFunction {
    public double r;
    public double c;

    public Gaussian(double radius, double center) {
        r = radius;
        c = center;
    }
    @Override
    public double function(double x) {
        double d = (x - c) * (x - c); 
        y = Math.exp(-((d) / (r * r)));  

        return y;    
}
    @Override
    public double functionDerivative(double x) {

        double d = (x - c) * (x - c); 
        double temp = Math.exp(- ((d) / (r * r)));
        double y = ((2 * c * temp) / (r * r)) - ((2 * x * temp) / (r * r));  

        return y;    
    }    
}
