package Math.ActivationFunctions;
public class HyperbolicTangent extends AbstractFunction {
    /*
    Fast tanh from http://www.musicdsp.org/showone.php?id=238
    
    pade approximation
    */
    @Override
    public double function(double x) {
        if( x < -3 )
            y = -1;
        else if( x > 3 )
            y = 1;
        else
            y = x * ( 27 + x * x ) / ( 27 + 9 * x * x );
        return y;   
    }
    @Override
    public double functionDerivative(double x) {
        double y = function(x);
        y*=y;
        y = 1 - y;
        return y;    
    }
    
}
