package Math.ActivationFunctions;
public class Sigmoid extends AbstractFunction {
    @Override
    public double function(double x) {
        return 1 / (1 + Math.exp(-x));
    }
    @Override
    public double functionDerivative(double x) {
        y = function(x);
        y *= 1 - y;
        return y;
    }
}
