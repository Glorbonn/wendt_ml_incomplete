package Math;
import DriverPack.Driver;
import java.util.Random;
/**
 *
 * @author Ross Wendt
 */
public class Matrix {
    private final int rows;
    private final int columns;
    private double[][] matrixValues;
    /*
    initializes a matrix based on a 2D double array
    */
    public Matrix(double[][] initialMatrixValues){
        rows = initialMatrixValues.length;
        columns = initialMatrixValues[0].length;
        //matrixValues = new double[rows][columns];
        matrixValues = initialMatrixValues;
        //for(int i = 0; i < matrixValues.length; i++){
        //    System.arraycopy(initialMatrixValues[i], 0, matrixValues[i], 0, matrixValues[0].length);
        //}
    }
    /*
    initializes a Matrix based on 1D double array
    */
    public Matrix(double[] initialMatrixValues){
        rows = 1;
        columns = initialMatrixValues.length;
        matrixValues = new double[rows][columns];
        matrixValues[0] = initialMatrixValues;
        //System.arraycopy(initialMatrixValues, 0, matrixValues[0], 0, matrixValues[0].length);
    }   
    public int getRows(){
        return rows;
    }   
    public int getColumns(){
        return columns;
    }   
    public double[][] getArray(){
        return matrixValues;
    }   
    public double[][] getMatrixValues(){
        return matrixValues;
    }
    public void setMatrixValues(int index1, int index2, double value) {
        matrixValues[index1][index2] = value;
    }
    /*
    matrix transposition
    */
    public Matrix transpose() {
        double[][] resultMatrix = new double[this.getColumns()][this.getRows()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                resultMatrix[j][i] = this.getArray()[i][j];
            }
        }
        return new Matrix(resultMatrix);
    }
    /*
    matrix addition
    */
    public Matrix madd(Matrix b) {
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH");
        }
        //double[][] zeroMatrix = new double[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                matrixValues[i][j] = this.getArray()[i][j] + b.getArray()[i][j];
            }
        }
        return this;
    }
    /*
    adds bias value to all elements of target matrix
    */
    public Matrix addBiasMatrices(Matrix bias) {
        //double[][] zeroMatrix = new double[this.getRows()][this.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < bias.getColumns(); i++) {
            for (int j = 0; j < this.getRows(); j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    matrixValues[j][k] = this.getArray()[j][k] + bias.getArray()[0][i];
                }
            }
        }
        return this;
    }
    /*
    randomly initializes matrix with upperbound
    */
    public Matrix init_rand(double upperBound) {
        Random randomGenerator = new Random();
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                double lowerBound = 0;
                double randomValue = lowerBound + (upperBound - lowerBound) * randomGenerator.nextDouble();
                this.getArray()[i][j] = randomValue;
            }
        }
        return this;
    }
    /*
    hadamard product, also called element-wise product
    */
    public Matrix hadProd(Matrix b) {
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH HADAMARD");
        }
        //double[][] zeroMatrix = new double[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                matrixValues[i][j] = this.getArray()[i][j] * b.getArray()[i][j];
            }
        }
        return this;
    }
    /*
    matrix multiplication
     */
    public Matrix mmul( Matrix b) {
        if (this.getColumns() != b.getRows()) {
            System.out.println("MATRIX MISMATCH ON MULTIPLY");
            System.out.println(this.getColumns() + " " + this.getRows() + "\n" + b.getColumns() + " " + b.getRows());
        }
        double[][] resultMatrix = new double[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix[0].length; j++) {
                for (int k = 0; k < this.getColumns(); k++) {
                    resultMatrix[i][j] += (this.getArray()[i][k] * b.getArray()[k][j]);
                }
            }
        }
        return new Matrix(resultMatrix);
    } 
    /*
    matrix subtraction
    */
    public Matrix msub(Matrix b) {
        if (this.getRows() != b.getRows() || this.getColumns() != b.getColumns()) {
            System.err.println("ERROR MATRIX MISMATCH");
        }
        //double[][] zeroMatrix = new double[this.getRows()][b.getColumns()];
        //Matrix resultMatrix = new Matrix(zeroMatrix);
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                matrixValues[i][j] = this.getArray()[i][j] - b.getArray()[i][j];
            }
        }
        return this;
    }
    /*
    scalar multiplication
    */
    public Matrix smul(double scalar) {
        for (int i = 0; i < this.getRows(); i++) {
            for (int j = 0; j < this.getColumns(); j++) {
                this.getArray()[i][j] = this.getArray()[i][j] * scalar;
            }
        }
        return this;
    }

    public void setVals(double[][] targOutArray) {
        this.matrixValues = targOutArray;
    }
}