package NeuralNet;
import DriverPack.Driver;
import Math.ActivationFunctions.AbstractFunction;
import Math.Matrix;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
public final class MatrixNeuralNet extends NetworkInterface {

    private final double initWeight;
    private final double initBias;
    public final double eta;
    public final double exponent;
    public final double momentumParameter;
    
    public final double epochLimit;
    

    private boolean isHiddenLayerCountZero;
    public Matrix input;
    public Matrix output;
    public Matrix targetOutput;
    public Matrix[] W;
    public Matrix[] bias;
    private final Matrix[] S;
    public final Matrix[] Z;
    public final Matrix[] F;
    public final Matrix[] D;
    public final Matrix[] lastW;
    public final Matrix[] lastBias;
    private AbstractFunction funcInterface;
    private TrainingMethodInterface trainInterface;

    // initialize the Matrix
    public MatrixNeuralNet(double[][] input, double[][] targetOutput, int[] hiddenLayers, double upperBoundInitializationWeight, 
            double upperBoundInitializationBias, double eta, double exponent, int epochLimit, double momentumParameter, int inEpochLimit, 
            AbstractFunction inActivationFunctionInterface,
            TrainingMethodInterface inTrainingMethodInterface) {
        this.eta = eta;
        this.exponent = exponent;
        this.epochLimit = epochLimit;
        this.initWeight = upperBoundInitializationWeight;
        this.initBias = upperBoundInitializationBias;
        this.momentumParameter = momentumParameter;
        funcInterface = inActivationFunctionInterface;
        trainInterface = inTrainingMethodInterface;
        epochLimit = inEpochLimit;        
        this.input = new Matrix (input);
        this.output = new Matrix(new double[Driver.outputSize]);
        this.targetOutput = new Matrix(targetOutput).transpose();
        isHiddenLayerCountZero = (hiddenLayers.length == 0);
        initializeWeights(hiddenLayers);
        lastW = new Matrix[W.length];
        for (int i = 0; i < W.length; i++) {
            lastW[i] = new Matrix(new double[W[i].getRows()][W[i].getColumns()]);
        }
        lastBias = new Matrix[W.length];
        for (int i = 0; i < bias.length; i++) {
            lastBias[i] = new Matrix(new double[bias[i].getRows()][bias[i].getColumns()]);
        }
        S = new Matrix[hiddenLayers.length];
        Z = new Matrix[hiddenLayers.length];
        F = new Matrix[hiddenLayers.length];
        D = new Matrix[hiddenLayers.length + 1];
    }

    // randomly initialize the node and bias node weights
    @Override
    public void initializeWeights(int[] hiddenLayers) {
        int layersLength = hiddenLayers.length;
        W = new Matrix[layersLength + 1];
        W[0] = new Matrix(new double[this.input.getColumns()][hiddenLayers[0]]);
        for (int i = 1; i < hiddenLayers.length; i++) {
            W[i] = new Matrix(new double[hiddenLayers[i - 1]][hiddenLayers[i]]);
        }
        W[W.length - 1] = new Matrix(new double[hiddenLayers[layersLength-1]][Driver.outputSize]);
        for (int i = 0; i < W.length; i++) {
            W[i] = W[i].init_rand(initWeight);
        }
        bias = new Matrix[layersLength + 1];
        for (int i = 0; i < bias.length - 1; i++) {
            bias[i] = new Matrix(new double[1][hiddenLayers[i]]);
        }
        bias[bias.length - 1] = new Matrix(new double[1][output.getColumns()]);
        for (int i = 0; i < bias.length; i++) {
            bias[i] = bias[i].init_rand(initBias);
        }
    }  
    @Override
    public double feedForward(Matrix TrainData) {
        S[0] = (TrainData.mmul(W[0])).addBiasMatrices(bias[0]);
        Z[0] = funcInterface.apply(S[0]);
        Matrix Q = funcInterface.applyDerivative(S[0]).transpose();
        F[0] = Q;
        for (int i = 1; i < S.length; i++) {
            S[i] = Z[i - 1].mmul(W[i]).addBiasMatrices(bias[i]);
            Z[i] = funcInterface.apply(S[i]);
            Matrix M = funcInterface.applyDerivative(S[i]).transpose();
            F[i] = M;
        }
        output = (Z[Z.length - 1].mmul(W[W.length - 1])).addBiasMatrices(bias[bias.length - 1]);
        return output.getArray()[0][0];
    } 
    public void setInputMatrix(double[] inputMatrix){
        input = new Matrix(inputMatrix);
    }  
    public Matrix getInputMatrix() {
        return input;
    }
    public Matrix getTargetOutputMatrix() {
        return targetOutput;
    }       
    public TrainingMethodInterface getTrainingMethodInterface() {
        return trainInterface;
    }   
    public Matrix getOutputMatrix() {
        return output;
    }   
    public double getEta() {
        return eta;
    }
}