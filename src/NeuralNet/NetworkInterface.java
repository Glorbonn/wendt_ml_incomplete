package NeuralNet;

import Math.Matrix;
import java.util.ArrayList;

/**
 *
 * @author Ross Wendt
 */
public abstract class NetworkInterface {
    public int epochLimit;
    
    public abstract void initializeWeights(int[] hiddenLayers);
    public abstract double feedForward(Matrix example);
    
    public void setEpochLimit(int in) {
        epochLimit = in;
    
    }
        
    public int getEpochLimit() {
        return epochLimit;
    }
}
