package NeuralNet;
import DriverPack.Driver;
import Math.Matrix;
import NeuralNet.TrainingMethod.Backpropagation;
import NeuralNet.TrainingMethod.Genetic.DifferentialEvolution;
import NeuralNet.TrainingMethod.Genetic.GeneticInterface;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
public class NeuralNetDriver {     
    int index = 0;
    int k = 1;
    double lastSum = 0;
    public int epoch;
    public static int outputStep;
    boolean runWithOutput = true;
    MatrixNeuralNet Net;
    ArrayList<Matrix> train;
    ArrayList<double[][]> toShuffle;
    ArrayList<Matrix> test;
    long Time;
    double timeElapsed;
    public NeuralNetDriver(MatrixNeuralNet inNeuralNet, int outputStep) {
        Net = inNeuralNet;
        NeuralNetDriver.outputStep = outputStep;
        toShuffle = partitionData();  
                
        train = new ArrayList<>();
        for (int i = 0; i < toShuffle.size(); i++) {
            Collections.shuffle(asList(toShuffle.get(i)), new Random(System.nanoTime()));
        }
        for (double[][] d : toShuffle) {
            Matrix M = new Matrix(d);
            train.add(M);
        }

        test = new ArrayList<>();
    }   
    public void runTest(boolean doOutput) {
        if ( doOutput == true ) {
            runWithOutput();
        } else {
            //NOT IMPLEMENTED runTestWithoutOutput();
        }
    }    
    public void runWithOutput() {
            //data for regression is random by default so can be considered pre-shuffled
            // otherwise, SHUFFLE DATA HERE!!!!!!!!!!!!!!! (which will need to be done with text input)
            
        for (int epoch = 0; epoch < Net.getEpochLimit(); epoch++) {
            //System.out.println("Epoch" + epoch + ":");
            runNeuralNet();
        }
    }        
    private ArrayList<double[][]> partitionData() {
        int numFolds = Driver.k;
        double[] d;
        ArrayList<double[][]> A = new ArrayList<>();
        int dataLengthCheck = Driver.xDataSet.length;
        int dataLengthCheck2 = Driver.xDataSet[0].length;
        int dataDim = Driver.xDataSet[0].length;
        int dataLength = Driver.xDataSet.length/numFolds;
        for (int i = 0; i < numFolds; i++ ) {
            double[][] dub = new double[dataLength][dataDim];
            for (int j = 0; j < dataLength; j++ ) {
                for (int k = 0; k < dataDim; k++ ) {
                    dub[j][k] = Driver.xDataSet[j][k];
                }
            }
            //Matrix M = new Matrix(dub);
            A.add(dub);
        }
        return A;
    }
    public void runNeuralNet() {
        double err = 0;
        //epoch = 0;
        TrainingMethodInterface T = Net.getTrainingMethodInterface();
        ArrayList<Double> testingError = new ArrayList<>();
        ArrayList<Double> trainingError = new ArrayList<>();
        //Collections.shuffle(train.get(0), new Random(System.nanoTime()));
        //System.out.print("TIME");
        do {
        //for (int epoch = 0; epoch < Driver.epochLimit; epoch++) {
            if (epoch % outputStep == 0 ) {
                System.out.println("EPOCH " + epoch + ":");
            }
            for (k = 0; k <= Driver.k - 1; k++ ) {
                Matrix testing = train.get(k);
                train.remove(testing);
                test.add(testing);
                
                Time = System.currentTimeMillis();
                //T.applyMethod(train);//, Net.output, k);
                
                if (Driver.trainingMethod instanceof Backpropagation ) {
                    for (int i = 0; i < train.size(); i++ ) {
                        for (int j = 0; j < train.get(i).getMatrixValues().length; j++ ) {
                             Matrix example = new Matrix(train.get(i).getArray()[j]);
                             //System.out.println(i);
                             //System.out.println(j);
                             Net.output.getArray()[0][0] = Net.feedForward(example);
                             T.applyMethod(example, Net.output, index); 
                             //index+=1;
                         }
                     }  
                    index += train.get(0).getArray().length;
                    index = index % (Driver.k * train.get(0).getArray().length);
                } else {
                //Select best for GAs
                    if (Driver.trainingMethod instanceof GeneticInterface) {
                        index += train.get(0).getArray().length;
                        T.applyMethod(null, null, index); //both null values are not used, as well as the index
                        Driver.trainingMethod.selectBest();
                    }
                    index = index % (Driver.k * train.get(0).getArray().length);
                }

                outputErr(k, test, train, epoch);
                                
                //testingError.add(getError(test));
                //testingError.add(getError(train));
                train.add(testing);
                //errorList.add(getError(train));
                test.remove(testing);
                //errorList.clear();

            }

            //outputErr2(testingError, trainingError, epoch);
            testingError.clear();
            trainingError.clear();
            
            
            epoch++;
            //System.out.println(timeElapsed - 100.0);
        } while ( epoch < Driver.epochLimit);
    }   
    //private double getError(ArrayList<Matrix> in, int k, int epoch) {
    //    return getError(in);
    //}       
    public void printWeights() {
        System.out.println("WEIGHT MATRIX: ");
        for (Matrix M : Net.W) {
            for (double weightArrays[] : M.getArray()) {
                for (double weights : weightArrays) {

                    System.out.print(weights + "  ");
                }
                System.out.println();
            }
        }
    }
    public void outputErr(int notUsed, ArrayList<Matrix> test, ArrayList<Matrix> train, int epoch) {
        if (epoch % outputStep == 0) {
            //timeElapsed += ((double) System.currentTimeMillis() - (double) Time)/1000.0;
                //printWeights();
                //System.out.print("EPOCH " + epoch + "\tTraining Errors: ");
                double trainErr = getRMSE(train);
                System.out.format("k = %d \tTraining Error:%1.10e\t", k, trainErr);
                
                //double testSum = 0;
                //for (int i = 0; i < testError.size(); i++) {
                //    testSum += testError.get(i);
                //}
                //testSum = testSum / Driver.dimensionality;
                //if(train.equals(test)) 
                //    System.out.println("ERROR");
                int index2 = k * train.get(0).getArray().length;
                //System.out.println(index2);
                double testErr = getRMSETest(test, index2);
                
                //double testErr = getError(test);
                System.out.format("\tTesting Error:\t%1.10e\n",testErr/*, Net.data.getArray()[0][0], Net.targetOutput.getArray()[0][1]*/);               
                }

            }

    public static void setOutputStep(int in) {
        outputStep = in;
    }
    
    public ArrayList<Matrix> getTrain() {
        return train;
    }
    
    public ArrayList<Matrix> getTest() {
        return test;
    }
    public double getRMSE(ArrayList<Matrix> in) {
        double mean = 0;
        double sum = 0;
        double RMSE = 0;
        int index2 = 0;
        index2 = index2 % (Driver.k * train.get(0).getArray().length);

        ArrayList<double[]> errors = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            double[] error = new double[in.get(0).getArray().length];
            int length = in.get(0).getArray().length;
            for (int example = 0; example < in.get(0).getArray().length; example++) {
               index2++;
               //index+=1;
               //System.out.println(index);
               double targetOutput = Net.targetOutput.getArray()[0][example+index2];
               Matrix exampleMatrix = new Matrix(in.get(i).getArray()[example]);
               Net.feedForward(exampleMatrix);
               double output = Net.output.getArray()[0][0]; 
               error[example] = targetOutput - output;
            }
            errors.add(error);
        }
        //Matrix errorPartOne = new Matrix(error);
        //Matrix squareError = new Matrix(errorPartOne.getArray());
        //Matrix squareErrorResult = new Matrix(errorPartOne.getArray());
        ArrayList<Double> squares = new ArrayList<>();
        for (int i = 0; i < errors.size(); i++) {
            for (double d : errors.get(i)) {
                squares.add(Math.abs(d*d)); //just to make sure
            }
        }
        
        for (double d : squares) {
            sum+= d;
        }
        
        /*for (int i = 0; i < errorPartOne.getRows(); i++) {
            for (int j = 0; j < errorPartOne.getColumns(); j++) {
                squareErrorResult.getArray()[i][j] = squareError.getArray()[i][j] * squareError.getArray()[i][j];
            }
        }*/
        /*for (double[] array : squareErrorResult.getArray()) {
            for (double d : array) {
                sum += d;
            }
        }*/
        mean = sum / (double) squares.size();
        //System.out.println();
        RMSE = Math.pow(mean, 0.5);
        //index = 0;
        //System.out.println("size: " + in.size() + "| sum: " + sum + " |RMSE: " + RMSE);
        return RMSE;
    }
    
    public double getRMSETest(ArrayList<Matrix> in, int index2) {
        double mean = 0;
        double sum = 0;
        double RMSE = 0;
        //if (index2 < 0 ) {
        //    index2 += Driver.k * train.get(0).getArray().length;
        //}

        ArrayList<double[]> errors = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            double[] error = new double[in.get(0).getArray().length];
            int length = in.get(0).getArray().length;
            //System.out.println(length);
            for (int example = 0; example < length; example++) {
               //index2++;
               //index+=1;
               double targetOutput = Net.targetOutput.getArray()[0][index2+example];
               Matrix exampleMatrix = new Matrix(in.get(i).getArray()[example]);
               Net.feedForward(exampleMatrix);
               double output = Net.output.getArray()[0][0]; 
               error[example] = targetOutput - output;
            }
            errors.add(error);
        }
        //Matrix errorPartOne = new Matrix(error);
        //Matrix squareError = new Matrix(errorPartOne.getArray());
        //Matrix squareErrorResult = new Matrix(errorPartOne.getArray());
        ArrayList<Double> squares = new ArrayList<>();
        for (int i = 0; i < errors.size(); i++) {
            for (double d : errors.get(i)) {
                squares.add(Math.abs(d*d)); //just to make sure
            }
        }
        
        for (double d : squares) {
            sum+= d;
        }
        
        /*for (int i = 0; i < errorPartOne.getRows(); i++) {
            for (int j = 0; j < errorPartOne.getColumns(); j++) {
                squareErrorResult.getArray()[i][j] = squareError.getArray()[i][j] * squareError.getArray()[i][j];
            }
        }*/
        /*for (double[] array : squareErrorResult.getArray()) {
            for (double d : array) {
                sum += d;
            }
        }*/
        mean = sum / (double) squares.size();
        //System.out.println();
        RMSE = Math.pow(mean, 0.5);
        //index = 0;
        //System.out.println("size: " + in.size() + "| sum: " + sum + " |RMSE: " + RMSE);
        return RMSE;
    }
}

