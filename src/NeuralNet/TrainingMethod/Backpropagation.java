package NeuralNet.TrainingMethod;
import DriverPack.Driver;
import Math.Matrix;
import NeuralNet.MatrixNeuralNet;
import static NeuralNet.TrainingMethod.TrainingMethodInterface.netDriver;
import static NeuralNet.TrainingMethod.TrainingMethodInterface.test;
import static NeuralNet.TrainingMethod.TrainingMethodInterface.train;
import java.util.ArrayList;
public class Backpropagation implements TrainingMethodInterface {   
    int counter = 0;
    double targOutArray[][];
    Matrix targetOutput;
    public Backpropagation() {
        targOutArray = new double [1][Driver.outputSize];
        targetOutput = new Matrix(targOutArray);
    }
    @Override
    public void applyMethod(Matrix exampleIn, Matrix notUsed, int index ) {
        Matrix[] D = Net.D;
        Matrix[] F = Net.F;
        Matrix[] W = Net.W;    

        targOutArray[0][0] = Net.targetOutput.getArray()[0][index];
        targetOutput.setVals(targOutArray);
        //Matrix targetOutput = new Matrix(targOutArray);        
        //System.out.format("Target Output: %1.4e\t Net Output: %1.4e\n", targOutArray[0][0], data.getArray()[0][0]);
        //double outArray[][] = new double[1][1];
        //outArray[0][0] = Net.data.getArray()[0][0];
        //Matrix data = new Matrix(outArray);       
        D[D.length - 1] = (Net.output.msub(targetOutput)).transpose(); 
        for (int i = D.length - 2; i > -1; i--) {
            D[i] = F[i].hadProd(W[i+1].mmul(D[i+1]));
        }
        updateWeights(Net, exampleIn);
        //printInfo();
    }
    public void updateWeights(MatrixNeuralNet neuralNet, Matrix example) {
        Matrix[] D = neuralNet.D;
        Matrix[] W = neuralNet.W;
        Matrix[] Z = neuralNet.Z;
        Matrix[] lastW = neuralNet.lastW;
        double eta = Net.eta; // Math.pow(Net.exponent, Net.eta/*/Net.epochLimit*/);
        double momentum = Net.momentumParameter;
        //Matrix deltaW = D[0].mmul(example).mmul(neuralNet.Z[0]).transpose().madd(lastW[0]).smul(-1.0*eta);
        //Matrix term1 = D[0].mmul(example);
        //Matrix term2 = term1.mmul(neuralNet.Z[0]).transpose();
        //Matrix term3 = 
        Matrix deltaW = (D[0].mmul(example).transpose()).madd(lastW[0]).smul(-1.0*eta);
        lastW[0] = deltaW;
        W[0] = W[0].madd(deltaW);
        for (int i = 1; i < neuralNet.W.length; i++) {
            //deltaW = D[i].mmul(neuralNet.Z[i-1]).transpose().madd(lastW[i]).smul(-1.0*eta);//(deltaWeight(neuralNet.Z[i - 1], D[i])).madd(lastW[i].smul(eta));
            deltaW = D[i].mmul(neuralNet.Z[i-1]).transpose().madd(lastW[i]).smul(-1.0*eta);
            deltaW = deltaW.madd(lastW[i].smul(momentum));
            lastW[i] = deltaW;
            W[i] = neuralNet.W[i].madd(deltaW);
        }
    }    
    
    /*public void printInfo() {
        netDriver.getError(train);
        netDriver.getError(test);
        if (counter % Driver.outputStep == 0 ) {
            System.out.format("\t%d\t Training Error: %1.10e\t Testing Error: %1.10e \n", counter, netDriver.getError(train), netDriver.getError(test));
            //System.out.printf("\t d \n", counter);
        }
        counter+=1;
    }*/

    /*
    finds delta for weights
     */
    /*public Matrix deltaWeight(Matrix zMatrix, Matrix M) {
        double eta = Driver.eta;
        Matrix deltaWeightMatrix = M.mmul(zMatrix).transpose();
        return deltaWeightMatrix;
    }*/

    @Override
    public void selectBest() {
        //not used
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
