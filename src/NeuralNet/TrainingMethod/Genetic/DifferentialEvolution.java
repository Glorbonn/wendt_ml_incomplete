package NeuralNet.TrainingMethod.Genetic;
import DriverPack.Driver;
import Math.Matrix;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
/**
 *
 * @author Ross Wendt
 */
public class DifferentialEvolution extends GeneticAbstract {
    //ArrayList<Matrix[]> Population = new ArrayList<>();
    int lengthWeights = Driver.hiddenLayers.length;
    int lengthMatrix = Driver.dataSetSize;   
    boolean init = true;
    //int populationSize = Driver.populationSize;
    //ArrayList<double[]> Population = new ArrayList<>();
    //Matrix[] examples = new Matrix[Driver.populationSize];
    //Matrix[] tempMatrix = new Matrix[Driver.populationSize];
    //Matrix[] candidates = new Matrix[Driver.populationSize];
    
    //public DifferentialEvolution() {
    //    for (int counter = 0; counter < populationSize; counter++ ) {
    //        examples[counter] = new Matrix(new double[Driver.dimensionality]);
    //        tempMatrix[counter] = new Matrix(new double[Driver.dimensionality]);
    //        candidates[counter] = new Matrix(new double[Driver.dimensionality]);
    //    }
    //}
    
    @Override
    public void applyMethod(Matrix exampleIn, Matrix output, int index) {
        if (init) {
            Population = initializePopulation(Driver.populationSize);
            for (int i = Driver.populationSize - 1; i < Driver.populationMax; i++) {
            //printInfo();
            Matrix[] sample = Population.get(i);
            ArrayList<Matrix[]> selectThreeRandom = randomSelect(3, sample);     
            Matrix[] mutant = mutate(selectThreeRandom);
            Matrix[] crossover = crossover(sample, mutant);
            Population.add(crossover); // replaces P(i) in P with mutant/ crossover if F(P(q)) < F(P(r)).
            //pop_indexer ++; 
            //System.out.println(i);
        }
            //System.out.println("DONE");
            init = false;
        }
        //if (pop_indexer < Population.size()) {
            for (int i = 0; i < Population.size(); i++ ) {
                //printInfo();
                Matrix[] sample = Population.get(i);
                ArrayList<Matrix[]> selectThreeRandom = randomSelect(3, sample);     
                Matrix[] mutant = mutate(selectThreeRandom);
                Matrix[] crossover = crossover(sample, mutant);
                replacement(sample, mutant, crossover, exampleIn, index);
                //System.out.println(i);
            }
        //printInfo();
        //selectBest(exampleIn, index);
        //} else {
        //    pop_indexer = 0;//if we've hit the population size, reset the pop_indexer to 0, so we begin looping back through the individuals in the population
        //}
    }
    
    public ArrayList<Matrix[]> randomSelect(int numToSelect, Matrix[] sample) {
        Set<Matrix[]> numUnique = new HashSet<>();
        ArrayList<Matrix[]> list = new ArrayList<>();
        
        while ( numUnique.size() < numToSelect + 1) {
            Random rand = new Random();
            int index = rand.nextInt(Population.size());
            Matrix[] selection = Population.get(index);
            numUnique.add(selection);   
        }
        list.addAll(numUnique);
        return list;
    }
    
    private Matrix[] crossover(Matrix[] sample, Matrix[] mutant) {
        Matrix[] crossover = sample.clone();
        double rate = Driver.crossoverRate;
        Random rand = new Random();        
        for (int i = 0; i < mutant.length; i++ ) {
            double next = rand.nextDouble();
            if (next < rate) {
                crossover[i] = mutant[i];
            } else {
                continue;
            }
        }
        return crossover;
    }
    
    private Matrix[] mutate(ArrayList<Matrix[]> in) {
        int length = Net.W.length;
        Matrix[] rand1 = in.get(0);
        Matrix[] rand2 = in.get(1);
        Matrix[] rand3 = in.get(2);
        //Matrix[] mutant = new Matrix();
        double beta = Driver.beta;

        Matrix[] mutant = new Matrix[length];
        //initialize mutant matrix
        for (int i = 0; i < length; i++) {
            Matrix L = new Matrix(Net.W[i].getArray());
            L = L.init_rand(Driver.upperBoundWeight); //these will be overwritten
            mutant[i] = L;
        }
        
        Matrix L = new Matrix(Net.W[0].getArray());
        L.init_rand(Driver.upperBoundWeight);
        
        for (int i = 0; i < length; i++) {
            mutant[i] = rand1[i].madd((rand2[i].msub(rand3[i])).smul(beta));
        
        }
        return mutant;
    }
}
