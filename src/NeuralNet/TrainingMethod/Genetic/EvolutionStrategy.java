package NeuralNet.TrainingMethod.Genetic;

import DriverPack.Driver;
import Math.Matrix;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ross Wendt
 */
public class EvolutionStrategy extends GeneticAbstract {
    boolean init = true;
    
     @Override
    public void applyMethod(Matrix exampleIn, Matrix output, int index) {
        if (init) {
            Population = initializePopulation(Driver.populationSize);
            for (int j = Population.size(); j < Driver.populationMax; j++){
                if (Population.size() > Driver.populationMax) {
                    break;
                }
                //System.out.println(j);
                ArrayList<Matrix[]> parents = randomSelect(Driver.mu);     
                ArrayList<Matrix[]> children = crossover(parents);
                ArrayList<Matrix[]> mutants = mutate(children);
                for (int i = 0; i < mutants.size(); i++ ) {
                    Population.add(mutants.get(i));
                    //System.out.println(mutants.size());
                }
                //System.out.println(Population.size());
                //System.out.println("DONE");
        }
            //System.out.println("DONE");
            init = false;
        }

        
        for (int i = 0; i < Population.size(); i++ ) {
            //printInfo();
            ArrayList<Matrix[]> parents = randomSelect(Driver.mu);     
            ArrayList<Matrix[]> children = crossover(parents);
            ArrayList<Matrix[]> mutants = mutate(children);
            replacement(mutants, parents, exampleIn, index);
            //System.out.println(Population.size());
        }
        //printInfo();
        //selectBest(exampleIn, index);
    }
    
    private ArrayList<Matrix[]> mutate(ArrayList<Matrix[]> in) {
        ArrayList<Matrix[]> mutants = new ArrayList<>();
        for (int i = 0; i < in.size(); i++) {
            mutants.add(in.get(i).clone());
        }
        
        for (int k = 0; k < mutants.size(); k++) {
            Random rand = new Random();    
            for (int i = 0; i < in.get(0).length; i++) {
                double next = rand.nextDouble(); //whether we mutate based on mute rate
                double mutate = rand.nextGaussian(); //how much we mutate, need to add one since nextGaussian has mean 0, sd 1
                //System.out.println(mutate);
                if (next < Driver.mutationRate) {
                    Matrix mutation = in.get(k)[i].madd(in.get(k)[i].smul(mutate));
                    mutants.get(k)[i] = mutation;
                }
            }
        }
        
        return mutants;
    }
    
    public void replacement(ArrayList<Matrix[]> mutants, ArrayList<Matrix[]> parents, Matrix exampleIn, int index) {
        ArrayList<Matrix> example = new ArrayList<>();
        example.add(exampleIn);
        for (int j = 0; j < parents.size(); j++) {
            Net.W = parents.get(j);
            double parentErr = netDriver.getRMSE(train);
            for (int k = 0; k < mutants.size(); k++) {
                Net.W = mutants.get(k);
                double mutantErr = netDriver.getRMSE(train);
                if (mutantErr < parentErr) {
                    Population.remove(parents.get(j));
                    Population.add(mutants.get(k));
                    break;
                }
            }
        }
        
        
        /*Net.W = sample;
        double sampleErr = netDriver.getError(train);
           
        Net.W = mutant;
        double mutantErr = netDriver.getError(train);
        
        Net.W = crossover;
        double crossoverErr = netDriver.getError(train);
        
        if (mutantErr < crossoverErr && mutantErr < sampleErr) {
            Population.remove(sample);
            Population.add(mutant);
        } else if (crossoverErr < mutantErr && crossoverErr < sampleErr) {
            Population.remove(sample);
            Population.add(crossover);
        }  */
    }
    
    /*
    Discrete recombination
    */
    private ArrayList<Matrix[]> crossover(ArrayList<Matrix[]> parents) { 
        ArrayList<Matrix[]> children = new ArrayList<>();
        double rate = Driver.crossoverRate;
        Random rand = new Random();
        
        for (int i = 0; i < Driver.lambda; i++ ) {
            children.add(parents.get(i%parents.size()));
        }
        
        for (int i = 0; i < children.size(); i++) {
            double next = rand.nextDouble();
            int randParent = rand.nextInt(parents.size());
            if (next < rate) {
                children.set(i,parents.get(randParent));
            }
        }
        return children;
    }
}
     

