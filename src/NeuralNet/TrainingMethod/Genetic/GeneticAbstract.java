package NeuralNet.TrainingMethod.Genetic;

import DriverPack.Driver;
import Math.Matrix;
import static NeuralNet.TrainingMethod.TrainingMethodInterface.Net;
import static NeuralNet.TrainingMethod.TrainingMethodInterface.train;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Ross Wendt
 */
public abstract class GeneticAbstract implements GeneticInterface{
    ArrayList<Matrix[]> Population = new ArrayList<>(); 
    int counter = 0;
    
    int pop_indexer = 0;
    /*
    The purpose of this indexer is to let the program select the next individual 
    from P every epoch. This is so the program can run based on epochs only, so 
    we can compare epochs between backprop and the GA, without running the GA
    |P| times for each epoch. I think this allows more direct comparisons be-
    tween the GAs and backprop, based purely on epoch size
 
    Side effect: if epoch limit < |P|, then we do not run through the
    entire population. Tests will be run with large enough epoch sizes, and
    low enough Population sizes, to avoid this.
    */

    
    /*
    Create a population of size specified, made of random weights.
    */
    @Override
    public ArrayList<Matrix[]> initializePopulation(int populationSize) {
        ArrayList<Matrix[]> Pop = new ArrayList<>();
        Matrix[] W = Net.W;
        int length = Net.W.length;
        for (int i = 0; i < populationSize; i++) {                       
            int j = 0;
            Matrix[] individual = new Matrix[length];
            for (Matrix M : Net.W) {
                Matrix L = new Matrix(Net.W[j].getArray());
                L = L.init_rand(Driver.upperBoundWeight);
                individual[j] = L;
                j++;
                if (j > length ) {
                    break;
                }
            }
            Pop.add(individual);
        }
        return Pop;
    }
    
        
    /*
    Elite selection from the current P(i), mutant, or crossover. Replacement
    of P(i) with most fit individual. Fit individual in from this function is then
    used in neural net. This method allows the program to 
    */
    @Override
    public void replacement(Matrix[] sample, Matrix[] mutant, Matrix[] crossover, Matrix notUsed2, int notUsed) {
        Net.W = sample;
        double sampleErr = netDriver.getRMSE(train);
           
        Net.W = mutant;
        double mutantErr = netDriver.getRMSE(train);
        
        Net.W = crossover;
        double crossoverErr = netDriver.getRMSE(train);
        
        if (Math.abs(mutantErr - crossoverErr) < .01) {
            mutantErr -= 0.1;
        }
        
        if (mutantErr < crossoverErr && mutantErr < sampleErr) {
            Population.remove(sample);
            Population.add(mutant);
        } else if (crossoverErr < mutantErr && crossoverErr < sampleErr) {
            Population.remove(sample);
            Population.add(crossover);
        }  
    }
    
       
    /*
    Randomly select n individuals (numToSelect) that are unique from P.
    */
    @Override
    public ArrayList<Matrix[]> randomSelect(int numToSelect) {
        Set<Matrix[]> numUnique = new HashSet<>();
        ArrayList<Matrix[]> list = new ArrayList<>();
        
        while ( numUnique.size() < numToSelect) {
            Random rand = new Random();
            int index = rand.nextInt(Population.size());
            Matrix[] selection = Population.get(index);
            numUnique.add(selection);   
        }
        list.addAll(numUnique);
        return list;
    }
    
    @Override
    public void selectBest() {
        //ArrayList<Matrix> a = new ArrayList<>();
        //a.add(example);
        Matrix[] smallest = Population.get(0);
        Net.W = smallest;
        double smallestErr = netDriver.getRMSE(train);
        //double smallestErr = netDriver.getError(a, index);
        for (int i = 1; i < Population.size(); i++) {
            Net.W = Population.get(i);
            double error = netDriver.getRMSE(train);
            //double error = netDriver.getError(a, index);
            if ( error < smallestErr ) {
                Net.W = Population.get(i);
                smallestErr = error;
            }
            
        }
    }
    
    /*public void printInfo() {
        netDriver.getError(train);
        netDriver.getError(test);
        if (counter % Driver.outputStep == 0 ) {
            System.out.format("\t%d\t Training Error: %1.10e\t Testing Error: %1.10e \n", counter, netDriver.getError(train), netDriver.getError(test));
            //System.out.printf("\t d \n", counter);
        }
        counter+=1;
    }*/
}
