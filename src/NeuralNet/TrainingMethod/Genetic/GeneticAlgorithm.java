package NeuralNet.TrainingMethod.Genetic;

import DriverPack.Driver;
import Math.Matrix;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Ross Wendt
 */
public class GeneticAlgorithm extends GeneticAbstract {
    int lengthWeights = Driver.hiddenLayers.length;
    int lengthMatrix = Driver.dataSetSize;   
    boolean init = true;
    
    @Override
    public void applyMethod(Matrix exampleIn, Matrix output, int index) {
        if (init) {
            Population = initializePopulation(Driver.populationSize);
            init = false;
            while (Population.size() < Driver.populationMax){
                //printInfo();
                ArrayList<Matrix[]> select = randomSelect(2);     
                Matrix[] parent1 = select.get(0);
                Matrix[] parent2 = select.get(1);
                ArrayList<Matrix[]> children = crossover(parent1, parent2);
                Matrix[] mutant1 = mutate(children.get(0));
                Matrix[] mutant2 = mutate(children.get(1));
                Population.add(mutant1);
                Population.add(mutant2);
            }
            //System.out.println("DONE");
        }

        
        for (int i = 0; i < Population.size(); i++ ) {
            //printInfo();
            ArrayList<Matrix[]> select = randomSelect(2);     
            Matrix[] parent1 = select.get(0);
            Matrix[] parent2 = select.get(1);
            ArrayList<Matrix[]> children = crossover(parent1, parent2);
            Matrix[] mutant1 = mutate(children.get(0));
            Matrix[] mutant2 = mutate(children.get(1));
            replacement(parent1, mutant1, mutant2, exampleIn, index);
            replacement(parent2, mutant1, mutant2, exampleIn, index);
        }
        //selectBest(exampleIn, index);
    }
    
    private Matrix[] mutate(Matrix[] in) {
        Matrix[] mutant = in.clone();
        Random rand = new Random();    
        for (int i = 0; i < in.length; i++) {
            double next = rand.nextDouble(); //whether we mutate based on mute rate
            double mutate = rand.nextGaussian(); //how much we mutate, need to add one since nextGaussian has mean 0, sd 1
            //System.out.println(mutate);
            if (next < Driver.mutationRate) {
                mutant[i] = in[i].madd(in[i].smul(mutate));
            }
        } 
        return mutant;
    }
    
    /*
    Single point crossover
    */
    private ArrayList<Matrix[]> crossover(Matrix[] parent1, Matrix[] parent2) { 
        ArrayList<Matrix[]> children = new ArrayList<>();
        Matrix[] child = parent1.clone();
        Matrix[] child2 = parent2.clone();
        double rate = Driver.crossoverRate;
        Random rand = new Random();
        
        for (int i = 0; i < parent1.length; i++ ) {
            double next = rand.nextDouble();
            child2[i] = parent1[i];
            if (next < rate) {
                for (int j = i; j < parent1.length - i; j++ ) {
                    child[j] = parent2[j];  
                }
                break;
            }
        }
        
        children.add(child);
        children.add(child2);
        return children;
    }
        
}
