package NeuralNet.TrainingMethod.Genetic;

import Math.Matrix;
import NeuralNet.TrainingMethod.TrainingMethodInterface;
import java.util.ArrayList;

/**
 *
 * @author Ross Wendt
 */
public interface GeneticInterface extends TrainingMethodInterface{ 
    public ArrayList<Matrix[]> initializePopulation(int populationSize);     
    public void replacement(Matrix[] sample, Matrix[] mutant, Matrix[] crossover, Matrix example, int index);     
    public ArrayList<Matrix[]> randomSelect(int numToSelect);
    public void selectBest();

}
