package NeuralNet.TrainingMethod;
import DriverPack.Driver;
import NeuralNet.MatrixNeuralNet;
import Math.Matrix;
import NeuralNet.NeuralNetDriver;
import java.util.ArrayList;
public interface TrainingMethodInterface {
    MatrixNeuralNet Net = Driver.getNeuralNet();
    ArrayList<Matrix> train = Driver.getNetDriver().getTrain();
    ArrayList<Matrix> test = Driver.getNetDriver().getTest();
    NeuralNetDriver netDriver = Driver.getNetDriver();
    //abstract void applyMethod(Matrix Example, Matrix output, int index); 
    abstract void applyMethod(Matrix exampleIn, Matrix output, int index);
    abstract void selectBest();
}
